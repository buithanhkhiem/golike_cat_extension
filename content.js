function click_get_job() {
    try {
        var btn_get_job = document.querySelector("#app > div > div:nth-child(1) > div.page-container > div:nth-child(3) > div.card.card-job-detail.mt-3.bg-gradient-8 > div > div > div > div.col.text-right > div")
        setTimeout(() => {
            btn_get_job.click()
        }, 3000);
    } catch (error) {
        console.log(error)
    }

}

const div = document.body

const config = {
    attributes: true,
    childList: true,
    characterData: true,
    subtree: true
};

const observer = new MutationObserver(function (MutationList) {
    for (const mutation of MutationList) {
        // console.log(window.location.href)
        // console.log(mutation)

        try {
            var popup = document.querySelector("body > div.swal2-container.swal2-center.swal2-fade.swal2-shown")
            if (mutation.type == "childList" || popup) {
                console.log("not have job")
                document.querySelector("body > div.swal2-container.swal2-center.swal2-fade.swal2-shown > div > div.swal2-actions > button.swal2-confirm.swal2-styled").click()
                // popup.hi
                // click_get_job()
            }
        } catch (error) {
            console.log("khong tin duoc nut nhan job")
        }
        if (window.location.href == "https://app.golike.net/jobs/instagram/job-detail") {
            try {
                let name_job = document.querySelector("#app > div > div:nth-child(1) > div.page-container > div:nth-child(1) > div > div > div:nth-child(1) > div:nth-child(1) > div.col-auto.px-0 > span").innerHTML
                console.log(name_job)
                let url_instagram = document.querySelector("#app > div > div:nth-child(1) > div.page-container > div.card.card-job-detail.mt-2 > div > div > div > div > div:nth-child(1) > div:nth-child(1) > a").href
                console.log(url_instagram)
                let buttonAll = document.getElementsByTagName("button")

                var create_tab = {
                    create: "ok",
                    name_job: name_job,
                    url_instagram: url_instagram,

                }

                chrome.runtime.sendMessage(create_tab)

                for (let i = 0; i < buttonAll.length; i++) {
                    if (buttonAll[i].innerText == "Hoàn thành") {
                        buttonAll[i].removeAttribute("disabled")
                    }
                }
            } catch (error) {
                console.log(error)
            }

        }
    }
})

observer.observe(div, config)